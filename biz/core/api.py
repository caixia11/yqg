import logging

#import requests

#from core.app_context import ApplicationContextMixin

"""
JsonApiSession根据我司后端接口的设计规范，封装了一些高级功能，如下：

- JsonApiSession
"""

__all__ = ['BizError', 'MalformedResponseError', 'UnexpectedContentTypeError', 'json', 'ApiSession']

#from core.log import abbr_name

def test_06():
    pass

class BizError(Exception):
    def __init__(self, code, detail):
        super(BizError, self).__init__(detail)
        self.__code = code
        self.__detail = detail

    def code(self):
        return self.__code

    def detail(self):
        return self.__detail

    def __str__(self):
        return '[%d] %s' % (self.__code, self.__detail)


class MalformedResponseError(Exception):
    def __init__(self, message):
        super(MalformedResponseError, self).__init__(message)


class UnexpectedContentTypeError(Exception):
    def __init__(self, expect, actual):
        self.expect = expect
        self.actual = actual

    def message(self):
        return "expect content type %s, actual content type %s" % (self.expect, self.actual)


"""

"""


def json(func):
    def parse_json_body(*args, **kwargs):
        # send request
        resp = func(*args, **kwargs)

        # parse json body
        body = resp.json()
        if not ('status' in body):
            raise MalformedResponseError('missing status from response: ' + body)
        else:
            status = body['status']
            if not ('code' in status):
                raise MalformedResponseError('missing code from response: ' + body)
            elif status['code'] not in non_error_codes:
                raise BizError(status['code'], status.get('detail', None))
            else:
                if 'body' in body:
                    return body['body']
                else:
                    body.pop('status', None)
                    return body

    return parse_json_body


"""
强烈建议先熟悉requests的基本用法: https://requests.readthedocs.io/en/master/
"""


class ApiSession(ApplicationContextMixin):

    def __init__(self, client, host):
        """
        :param client: 客户端信息，类型biz.core.client.Client
        :param host: api服务的地址，类型string
        """
        self.client = client
        self.host = host
        self.session = requests.session()
        self.logger = logging.getLogger(abbr_name(self))

    def __del__(self):
        if hasattr(self, 'session'):
            self.session.close()

    def get(self, path, **params):
        """
        将params透传给底层的session.get()
        params允许的取值参考Session的request方法
        """
        headers = self.client.headers()
        if 'headers' in params:
            headers.update(params['headers'])
        params['headers'] = headers
        resp = self.session.get(self.host + path, **params)
        self.__debug_request(resp.request)
        resp.raise_for_status()
        self.__debug_response(resp)
        return resp

    def post(self, path, **params):
        """
        将params透传给底层的session.post()
        params允许的取值参考Session的request方法
        """
        headers = self.client.headers()
        if 'headers' in params:
            headers.update(params['headers'])
        params['headers'] = headers
        resp = self.session.post(self.host + path, **params)
        self.__debug_request(resp.request)
        resp.raise_for_status()
        self.__debug_response(resp)
        return resp

    def __debug_request(self, req):
        self.logger.debug('{} {}\r\n{}\r\n\r\n{}\r\n'.format(
            req.method,
            req.url,
            '\r\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
            req.body
        ))

    def __debug_response(self, resp):
        self.logger.debug('{} {}\r\n{}\r\n\r\n{}\r\n'.format(
            resp.status_code,
            resp.reason,
            '\r\n'.join('{}: {}'.format(k, v) for k, v in resp.headers.items()),
            resp.content
        ))
